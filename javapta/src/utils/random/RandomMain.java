package utils.random;
import java.util.Random;
import java.util.Scanner;



public class RandomMain {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		long seed = sc.nextLong();
		int n = sc.nextInt();
		Random random = new Random(seed);
			
		double x,y;
		int z = 0;
		for (int i = 0; i < n; i++) {
			x = getRandomNumber(random);
			y = getRandomNumber(random);
			if (Math.sqrt(Math.pow(x,2)+Math.pow(y, 2))<1) {
				z++;
			}
		}
		System.out.println(1.0*z/n*4);
		sc.close();
		
		
	}

	private static double getRandomNumber(Random random) {
		return random.nextDouble()*2-1;
	}

}
